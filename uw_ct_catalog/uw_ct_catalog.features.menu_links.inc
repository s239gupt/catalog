<?php
/**
 * @file
 * uw_ct_catalog.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_catalog_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_catalog-categories:admin/structure/taxonomy/catalog_categories.
  $menu_links['menu-site-manager-vocabularies_catalog-categories:admin/structure/taxonomy/catalog_categories'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/catalog_categories',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'catalog categories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_catalog-categories:admin/structure/taxonomy/catalog_categories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_catalog-tags:admin/structure/taxonomy/uw_catalog_tags.
  $menu_links['menu-site-manager-vocabularies_catalog-tags:admin/structure/taxonomy/uw_catalog_tags'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_catalog_tags',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'catalog tags',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_catalog-tags:admin/structure/taxonomy/uw_catalog_tags',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('catalog categories');
  t('catalog tags');

  return $menu_links;
}
