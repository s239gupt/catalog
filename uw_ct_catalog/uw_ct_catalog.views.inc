<?php

/**
 * @file
 * Provides the view object type and associated methods.
 */

/**
 * Implements hook_views_data().
 */
/*function uw_ct_catalog_views_data() {

  $data = array();

  $data['feedbackform']['table']['group'] = t('feedbackform');

  $data['feedbackform']['table']['base'] = array(
    'title' => t('feedbackform'),
    'help' => t('Contains feedbackform records to Views.'),
  );

  // The feedback ID field
  $data['feedbackform']['feedback_id'] = array(
      'title' => t('Feedback ID'),
      'help' => t('The feedback ID.'),
      'field' => array(
          'handler' => 'views_handler_field_numeric',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
  );

  // The Feedback What were you looking for field
  $data['feedbackform']['feedback_what'] = array(
      'title' => t('What'),
      'help' => t('What were you looking for?'),
      'field' => array(
          'handler' => 'views_handler_field',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_string',
      ),
  );

  // The Feedback Where did you expect to find it field
  $data['feedbackform']['feedback_note'] = array(
    'title' => t('Note'),
    'help' => t('Where did you expect to find it?'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // The Feedback email field
  $data['feedbackform']['feedback_mail'] = array(
    'title' => t('Email'),
    'help' => t('May we contact you?'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // The created field
  $data['feedbackform']['created'] = array(
    'title' => t('Created'),
    'help' => t('The feedback form created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['feedbackform']['table']['join'] = array(
      'users' => array(
          'left_field' => 'uid',
          'field' => 'uid',
      ),
  );

  // The Uid field
  $data['feedbackform']['uid'] = array(
      'title' => t('User ID'),
      'help' => t('The feedback form from this user ID.'),
      'field' => array(
          'handler' => 'views_handler_field_user',
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
      'filter' => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'relationship' => array(
          'base' => 'users',
          'field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('Users'),
      ),
      'argument' => array(
          'handler' => 'views_handler_argument_node_uid',
          'numeric' => TRUE,
          'validate type' => 'uid',
      ),
  );

  $data['feedbackform']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // The Node ID field
  $data['feedbackform']['nid'] = array(
    'title' => t('Node ID'),
    'help' => t('The feedback form comment on node ID.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  return $data;

}*/
