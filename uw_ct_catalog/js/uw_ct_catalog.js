/**
 * @file
 * Adds the ability to filter the list based on entered text.
 */

(function ($) {

    Drupal.behaviors.uw_ct_catalog = {
        attach: function (context, settings) {
            if (!$('#a-z-filter').length) {
                $('#content .view-header').after('<div><input type="text" id="a-z-filter" placeholder="A-Z filter: enter name"></div><div id="a-z-nomatch">No matching articles.</div>');
                $('#a-z-filter').on('keyup', function () {
                    filter = $(this).val().toLowerCase();
                    if (filter == '') {
                        $('#block-system-main .view-display-id-catalog_all_attachment .views-row, .view-display-id-catalog_glossary_attachment, #block-system-main .view-display-id-catalog_all_attachment .view-content h3').show();
                        $('#a-z-nomatch').hide();
                    }
                    else {
                        $('#block-system-main .view-display-id-catalog_all_attachment .views-row, .view-display-id-catalog_glossary_attachment, #block-system-main .view-display-id-catalog_all_attachment .view-content h3').hide();
                        $('#block-system-main .view-display-id-catalog_all_attachment .views-row').filter(function () {
                            text = $(this).find('a').html().toLowerCase();
                            return text.indexOf(filter) > -1;
                        }).show().each(function () {
                            $(this).prevAll('h3').first().show();
                        });
                        if ($('#block-system-main .view-display-id-catalog_all_attachment .views-row:visible').length) {
                            $('#a-z-nomatch').hide();
                        }
                        else {
                            $('#a-z-nomatch').show();
                        }
                    }
                });
            }
        }
    };

}(jQuery));