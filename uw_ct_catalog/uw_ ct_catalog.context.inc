<?php
/**
 * @file
 *  uw_ct_catalog.context.inc
 */

/**
 * Implements hook_context_default_contexts()
 */
function uw_ct_catalog_context_default_contexts() {
    $export = array();
    $context = new stdClass();
    $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
    $context->api_version = 3;
    $context->name = 'uw_catalog';
    $context->description = 'Displays search for catalog';
    $context->tag = 'Content';
    $context->conditions = array(
        'path' => array(
            'values' => array(
                'catalog' => 'catalog',
                'catalog/all' => 'catalog/all',
                'catalog/by-audience' => 'catalog/by-audience',
                'catalog/popular' => 'catalog/popular',
                'catalog/search' => 'catalog/search',
            ),
        ),
    );
    $context->reactions = array(
        $context->reactions = array(
            'block' => array(
                'blocks' => array(
                    'uw_ct_catalog-catalog_search' => array(
                        'module' => 'uw_ct_catalog',
                        'delta' => 'catalog_search',
                        'region' => 'content',
                        'weight' => '-10',
                    ),
                ),
            ),
        ),
    );
    $context->condition_mode = 0;

    // Translatables
    // Included for use with string extractors like potx.
    t('Content');
    t('Displays search for catalog');
    $export['uw_catalog'] = $context;


    return $export;
}
